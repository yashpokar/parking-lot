from setuptools import setup, find_packages

setup(
	name='plot',
	version='0.1',
	description='Parking Lot System',
	url='https://gitlab.com/yashpokar/parking-lot',
	author='Yash Patel',
	author_email='hello@yashpokar.com',
	license='MIT',
	packages=find_packages(exclude=('tests', 'tests/*')),
	install_requires=(),
    scripts=['bin/parking-lot'],
	test_suite='nose.collector',
    tests_require=['nose'],
	zip_safe=False
)
