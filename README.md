# Parking Lot
> Simple Parking Lot Design

## Installation
```git clone https://gitlab.com/yashpokar/parking-lot```
```cd parking-lot```
- Install Python 3.6.9
- Install Virtualenv create venv with version 3.6.9
- refer to https://mothergeo-py.readthedocs.io/en/latest/development/how-to/venv.html based on whatever operating systems you're using
- Activate the virtualenv
```pip install .```

## Run manual testcases
```parking-lot --file "/home/ubuntu/Downloads/sample.txt"```

## To run testcases
```python setup.py test```

## Assumptions Made While Designing
- Driver should be at least eligible to driver the vehicle (at least 18 years old)
- While testing the commandline script, the input passed should be in sequence (like park will be provied after create_parking_lot)

## What we haven't done
- Starting slot number from zero, which is not the human preffered first number
