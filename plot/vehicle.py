class Vehicle(object):
    def __init__(self, registration_no: str, driver_age: int):
        self._registration_no = registration_no
        self._driver_age = driver_age

    @property
    def registration_no(self):
        return self._registration_no

    @property
    def driver_age(self):
        return self._driver_age

    def __str__(self):
        return f"""Vehicle Registration Number: {self._registration_no}
Driver's Age               : {self._driver_age}"""

