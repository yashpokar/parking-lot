class OutofCapacityError(ValueError):
    pass


class NotEligibleError(Exception):
    pass


class ValidationError(ValueError):
    pass
