import re
from .exc import OutofCapacityError, NotEligibleError, ValidationError
from .vehicle import Vehicle


class ParkingLot(object):
    def __init__(self, capacity, minimum_allowed_age_of_driver=18, vehicle_registration_number_pattern=r'\w{2}-\d{2}-\w{2}-\d{4}'):
        self._minimum_allowed_age_of_driver = minimum_allowed_age_of_driver
        self._vehicle_registration_number_pattern = re.compile(vehicle_registration_number_pattern)
        self._capacity = capacity
        self._availability = capacity
        self._slots = [-1] * capacity

    @property
    def capacity(self):
        return self._capacity

    def slots(self):
        return self._slots

    def count_available_slots(self):
        return self._availability

    def has_parking_slot_left(self) -> bool:
        return not not self.count_available_slots()

    def find_nearest_available_slot(self):
        for idx, slot in enumerate(self._slots):
            if slot == -1:
                return idx

        # TODO : message should be same, generalize this expection message
            # or extract to a method
        raise OutofCapacityError('We don\'t have enough space to park')

    def has_slot_available(self, slot_index):
        return self._slots[slot_index] != -1

    def _validate_vehicle_registration_no_or_throw_exception(self, registration_no):
        if not self._vehicle_registration_number_pattern.match(registration_no):
            raise ValidationError('Invalid vehicle registration number provided.')
        return True

    def assign_slot(self, slot_index, registration_no, driver_age):
        if driver_age < self._minimum_allowed_age_of_driver:
            raise NotEligibleError('You are not supposed to drive, we can not let you park your vehicle in our parking lot.')

        self._validate_vehicle_registration_no_or_throw_exception(registration_no)

        if self.has_slot_available(slot_index):
            raise OutofCapacityError(f'Slot {slot_index} is not availability')

        self._slots[slot_index] = Vehicle(registration_no=registration_no,
            driver_age=driver_age)
        self._availability = self._availability - 1

        # TODO : this should return the slot number, which is a basic need
        return True

    def release_slot(self, slot_index):
        self._slots[slot_index] = -1

        # TODO : deserves a shaperate method
        self._availability = self._availability + 1
        return True

    def park_vehicle(self, *args, **kwargs):
        """Entry of a vehicle

        Alias to 'assign_slot' for more verbose
        """

        if not self.has_parking_slot_left():
            raise OutofCapacityError('We don\'t have any parking slot left.')

        return self.assign_slot(self.find_nearest_available_slot(),
            *args, **kwargs)

    def withdraw_vehicle(self, slot_index, *args, **kwargs):
        """Exit of a vehicle

        Alias to 'release_slot' for more verbose
        """
        return self.release_slot(slot_index=slot_index, *args, **kwargs)

    def get_slot_details_by_vehicle_registration_no(self, registration_no):
        # TODO : what a great method name
        # TODO : we could use the dict while alloting slot, retrival could be fater in that case

        for idx, slot in enumerate(self._slots):
            if slot.registration_no == registration_no:
                return idx

        raise Exception('Vehicle could not found')

    def get_vehicle_details(self, slot_index):
        return str(self._slots[slot_index])

    def occupied_slots(self):
        for idx, slot in enumerate(self._slots):
            if slot == -1:
                continue
            yield idx, slot

    def get_vehicle_registration_no_by_drivers_age(self, driver_age: int):
        return [vehicle.registration_no for _, vehicle in self.occupied_slots() if vehicle.driver_age == driver_age]

    def get_slot_by_drivers_age(self, driver_age: int):
        # TODO : this could be generalized with upper one but I'm not going to do this time
        return [idx for idx, vehicle in self.occupied_slots() if vehicle.driver_age == driver_age]

    def get_slot_number_for_registration_number(self, registration_no):
        for idx, slot in self.occupied_slots():
            if slot.registration_no == registration_no:
                return idx

    def status(self):
        table = ''

        for _, slot in self.occupied_slots():
            table += f"""{slot}
------------------------------------------
"""
        return table.strip()

