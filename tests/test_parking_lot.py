import unittest
import plot


class TestParkingLot(unittest.TestCase):
    def test_parking_lot_creation(self):
        capacity = 6
        parking_lot = plot.ParkingLot(capacity=capacity)

        self.assertEqual(parking_lot.capacity, capacity)

    def test_slot_allotment_when_in_capacity(self):
        pl = plot.ParkingLot(capacity=6)
        could_park_successfully = pl.park_vehicle('KA-01-HH-1234', 21)

        self.assertTrue(could_park_successfully)

    def test_slot_allotment_when_out_of_capacity(self):
        pl = plot.ParkingLot(capacity=6)
        pl.park_vehicle('KA-01-HH-1234', 21)
        pl.park_vehicle('PB-01-HH-5026', 48)
        pl.park_vehicle('GJ-01-HH-3839', 39)
        pl.park_vehicle('CG-01-HH-0918', 63)
        pl.park_vehicle('MP-01-HH-1234', 53)
        pl.park_vehicle('MH-01-HH-3839', 57)

        with self.assertRaises(plot.OutofCapacityError):
            pl.park_vehicle('RJ-01-HH-3784', 27)

    def test_allotment_fails_when_drivers_ages_doesnt_meets_the_expectation(self):
        pl = plot.ParkingLot(capacity=6)
        pl.park_vehicle('KA-01-HH-1234', 21)

        with self.assertRaises(plot.NotEligibleError):
            pl.park_vehicle('GJ-01-HH-3839', 16)

    def test_allotment_fails_on_inalid_vehicle_registration_number_passed(self):
        pl = plot.ParkingLot(capacity=6)
        pl.park_vehicle('KA-01-HH-1234', 21)

        with self.assertRaises(plot.ValidationError):
            pl.park_vehicle('GJ-01HH-3839', 26)

    def test_availability_count_when_slot_allotment_is_only_involed(self):
        pl = plot.ParkingLot(capacity=6)
        pl.park_vehicle('KA-01-HH-1234', 21)
        pl.park_vehicle('PB-01-HH-5026', 48)
        pl.park_vehicle('GJ-01-HH-3839', 39)

        self.assertEqual(pl.count_available_slots(), 3)

    def test_availability_count_when_slot_allotment_is_only_involed(self):
        pl = plot.ParkingLot(capacity=6)
        pl.park_vehicle('KA-01-HH-1234', 21)
        pl.park_vehicle('PB-01-HH-5026', 48)
        pl.park_vehicle('GJ-01-HH-3839', 39)

        self.assertEqual(pl.count_available_slots(), 3)

    def test_availability_count_when_allotment_and_withdraw_are_involved(self):
        # TODO : refactor should be done
            # create a helper method for this
        pl = plot.ParkingLot(capacity=6)
        pl.park_vehicle('KA-01-HH-1234', 21)
        pl.park_vehicle('PB-01-HH-5026', 48)
        pl.park_vehicle('GJ-01-HH-3839', 39)
        pl.withdraw_vehicle(1)
        pl.withdraw_vehicle(2)

        self.assertEqual(pl.count_available_slots(), 5)

    def test_vehicle_withdraw_vehicle(self):
        pl = plot.ParkingLot(capacity=6)
        pl.park_vehicle('KA-01-HH-1234', 21)
        was_able_to_withdraw = pl.withdraw_vehicle(0)

        self.assertTrue(was_able_to_withdraw)

    def test_vehicle_registration_no_for_all_vehicles_parked_by_drivers_of_certain_age(self):
        pl = plot.ParkingLot(capacity=6)
        pl.park_vehicle('KA-01-HH-1234', 21)
        pl.park_vehicle('PB-01-HH-5026', 25)
        pl.park_vehicle('GJ-01-HH-3839', 25)
        pl.park_vehicle('CG-01-HH-0918', 63)
        pl.park_vehicle('MP-01-HH-1234', 53)
        pl.park_vehicle('MH-01-HH-3839', 57)

        vehicle_registration_numbers = pl.get_vehicle_registration_no_by_drivers_age(25)

        self.assertEqual(['PB-01-HH-5026', 'GJ-01-HH-3839'],
            vehicle_registration_numbers)

    def test_slot_details_by_vehicle_registration_no(self):
        pl = plot.ParkingLot(capacity=6)
        pl.park_vehicle('KA-01-HH-1234', 21)
        pl.park_vehicle('GJ-01-HH-3839', 25)
        pl.park_vehicle('CG-01-HH-0918', 63)
        pl.park_vehicle('MP-01-HH-1234', 53)

        self.assertEqual(2, pl.get_slot_details_by_vehicle_registration_no('CG-01-HH-0918'))

    def test_slot_by_drivers_age(self):
        pl = plot.ParkingLot(capacity=6)
        pl.park_vehicle('KA-01-HH-1234', 21)
        pl.park_vehicle('GJ-01-HH-3839', 28)
        pl.park_vehicle('CG-01-HH-0918', 28)
        pl.park_vehicle('MP-01-HH-1234', 53)

        self.assertEqual([1, 2], pl.get_slot_by_drivers_age(28))

    def test_occupied_slots(self):
        pl = plot.ParkingLot(capacity=6)
        pl.park_vehicle('KA-01-HH-1234', 21)
        pl.park_vehicle('GJ-01-HH-3839', 28)

        # TODO : test should contain any operation but for our case it seems okay
        self.assertEqual('KA-01-HH-1234', next(pl.occupied_slots())[1].registration_no)

    def test_vehicle_details_by_slot(self):
        pl = plot.ParkingLot(capacity=6)
        pl.park_vehicle('KA-01-HH-1234', 21)

        expected = """Vehicle Registration Number: KA-01-HH-1234
Driver's Age               : 21"""

        details = pl.get_vehicle_details(0)
        self.assertEqual(expected, details)

    def test_status(self):
        pl = plot.ParkingLot(capacity=6)
        pl.park_vehicle('KA-01-HH-1234', 21)
        pl.park_vehicle('GJ-01-HH-3839', 28)

        status = pl.status()
        self.assertIn('KA-01-HH-1234', status)
        self.assertIn('GJ-01-HH-3839', status)
        self.assertIn('21', status)
        self.assertIn('28', status)

    def test_get_slot_number_for_registration_number(self):
        pl = plot.ParkingLot(capacity=6)
        pl.park_vehicle('KA-01-HH-1234', 21)
        pl.park_vehicle('GJ-01-HH-3839', 28)

        slot_index = pl.get_slot_number_for_registration_number('GJ-01-HH-3839')
        self.assertEqual(1, slot_index)

    def test_minimum_driver_age(self):
        pl = plot.ParkingLot(capacity=6, minimum_allowed_age_of_driver=16)
        pl.park_vehicle('KA-01-HH-1234', 21)
        ok = pl.park_vehicle('GJ-01-HH-3839', 28)

        self.assertTrue(ok)

    def test_vehicle_registration_number_pattern(self):
        pl = plot.ParkingLot(capacity=6, vehicle_registration_number_pattern=r'\w{2}\d{2}\w{2}\d{4}')
        ok = pl.park_vehicle('KA01HH1234', 21)

        self.assertTrue(ok)
